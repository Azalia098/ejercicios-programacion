#include <stdio.h>
void escaleno(){
    
    float perimetro=0;
    float lado1;
 	float lado2;
 	float base; 
 	printf("Ingresa el tamanio de un lado: ");
 	scanf("%f",&lado1);
 	printf("Ingresa el tamanio de un lado: ");
 	scanf("%f",&lado2);
 	printf("Ingresa el tamanio de la base: ");
 	scanf("%f",&base);

 	perimetro=lado1+lado2+base;
 	printf("El perimetro del triangulo escaleno es: %f cm",perimetro);
 
}

void equilatero(){
    int LADO=3;
	float perimetro=0;
	float tamanio;
	printf("Ingresa el tamanio de uno de los lados\n");
	scanf("%f",&tamanio);
    perimetro=LADO*tamanio;
    
	printf("El perimetro del triangulo equilatero es: %f cm",perimetro);
}
void isosceles(){
    int LADO=2;
	float perimetro=0;
	float lado_lateral;
 	float base;
    printf("Ingresa el tamanio de uno de los lados laterales: ");
 	scanf("%f",&lado_lateral);
 	printf("Ingresa el tamanio de la base: ");
 	scanf("%f",&base);
    perimetro=LADO*lado_lateral+base;
    
	printf("El perimetro del triangulo isosceles es: %f cm",perimetro);
}

void main()
{
   int opcion=0;

	printf("Ingresa el numero de las siguientes opciones para el perimetro de un triangulo: \n1. Equilatero. \n2. Escaleno. \n3. Isosceles.\n");
	scanf("%i",&opcion);

	switch(opcion){
		case 1:
			equilatero();
			break;
		case 2:
			escaleno();
			break;
		case 3:
			isosceles();
			break;
		default:
			printf("Error, opcion no valida");
    }
}