#include <stdio.h>
 
void main()
{
   int numero=0;
   int suma=0;
   printf("Introduce un numero entre 1 y 50 para mostrar la suma de los numeros consecutivos:\n");
   scanf("%i",&numero);
   
   if(numero>=1 && numero<=50){
       for(int i = 0; i <= numero; i++){
			suma=suma+i;
       }

		printf("La suma de los numeros consecutivos del 1 al %i  es %i",suma,numero);
		
   }else{
       printf("Error, el numero debe ser entre 1 y 50 ");
   }
   
}
